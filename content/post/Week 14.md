---
title: Week 14
date: 2017-12-08
---

#### Maandag
Vandaag heb ik een enquette opgezet via Google Forms voor het eerste prototype. Ook heb ik de prototypes laten valideren. Dit duurde allemaal heel lang en zijn hiervoor meerdere uren aan kwijt geweest. Ook heb ik zelf een klein digitaal prototype opgezet waarmee bezoekers hun favoriete dingen kunnen swipen.
#### Dinsdag
Vanochtend hadden we weer hoorcollege. Vandaag heb ik het testplan laten valideren bij Pia. Deze is goedgekeurd. Ook heb ik nog gewerkt aan het High Fidelity prototype.
#### Woensdag
Op deze woensdag hebben we ons idee gepitcht aan Carlo, de opdrachtgever, docenten en de andere klasgenoten. Ook hebben we Feedback aan Carlo gevraagd wat hij van het idee vondt. Want we hadden een app bedacht, waar de opdrachtgever niet helemaal mee eens is. Dus hebben we besloten om het oude idee te schrappen. Ook heb ik samen met Dylan gewerkt aan de recap van het project.
