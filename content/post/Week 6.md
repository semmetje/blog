---
title: Week 6
date: 2017-10-06
---

#### Maandag
Vandaag op de eerste studiodag moesten we het definitieve spelconcept bedenken en hiervan een prototype maken. Ons definitieve spelconcept is een spel waarin de verschillende communicatie studenten proberen een zon groot mogelijk deel van Rotterdam zien te veroveren. Het team wat het grootse gedeelte van Rotterdam heeft veroverd wint. 

#### Dinsdag
Volgens het rooster zouden we vandaag een hoorcollege van Design Theory krijgen maar deze ging niet door. Dus hebben we vandaag verder gewerkt aan het spelconcept en voorbereid voor de presentatie.

#### Woensdag
De dag van de presentaties. Op deze woensdag moesten we de presentatie gegeven van ons spelconcept. Wij hadden de presentatie in de middag, deze hebben we gegeven aan medestudenten en docenten. De naam 010 Snaaiers vonden ze leuk en goed bedacht. Hier buiten hebben we ook andere feedback gehad en verwerkt.

#### Donderdag
Vandaag hadden we weer photoshop. Hierbij moest ik een menu maken met daarin verschillende submenu's. Dit is goed gelukt vind ik zelf.
