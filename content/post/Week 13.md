---
title: Week 13
date: 2017-12-01
---

#### Maandag
Vandaag hebben we de inviduele leerdoelen en user journey opgezet. 
#### Dinsdag
Deze dinsdag hebben we zoals gewoonlijk hoorcollege gehad en hebben we de userjourney digitaal gemaakt en verwerkt in Illustrator.
#### Woensdag
Vandaag hebben we onze low Fidelity ideeën besproken en een testplan opgesteld. Ook heb ik samen met Kayleigh de merkanalyse aangepast want deze was niet goedgekeurd.
