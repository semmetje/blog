---
title: Week 5
date: 2017-09-29
---

#### Maandag
Vandaag moesten we een ontwerpproces kaart maken van de eerste iteratie en een gedeelte van de tweede iteratie waar we die dag aan begonnen waren. Ook hebben we vandaag gebrainstormd over nieuwe spelconcepten.

#### Dinsdag
Op deze dinsdag hebben we als gebruikelijk hoorcollege gehad van Design Theory.

#### Woensdag
Op de tweede studiodag deze week hebben we onze spelconcepten besproken en nog een paar andere spellen gespeeld en daar spelanalyses van gemaakt. Deze analyses kunnen we dan gebruiken in ons eigen spel.

#### Donderdag
Om half 9 had ik weer het keuzevak Photoshop. Deze keer kregen we een oude verkreukelde afbeelding die we moesten "opknappen" zodat hij er weer goed en zonder kreukels eruit zag. Dit is een lastig karwei en kan nooit helemaal mooi worden gemaakt. Toch is het opknappen wel aardig gelukt vind ik zelf. 