---
title: Week 3
date: 2017-09-15
---

#### Maandag
En we beginnen weer. Vandaag hebben we binnen ons team elkaars bedachte concepten gepresenteerd. Dit was leuk om te doen. Uit al onze concepten hebben we verschillende onderdelen gepakt en omgezet in een definitief idee.
We hebben als laatste een taakverdeling gemaakt met wie wat moet doen. Ik heb voor de game de locaties bepaald waar de opdrachten worden uitgevoerd. 

#### Dinsdag
Weer een hoorcollege gehad over Design Theorie. Deze keer door een andere docent. Was weer zeer interessant. Normaal zou ik Workshop hebben vandaag alleen begint dat voor mij pas volgende week. In plaats daarvan heb ik mijn Moodboard en visueel onderzoek afgemaakt en ingeleverd.

#### Woensdag
Vandaag hebben we onze game verder uitbedacht en gedocumenteerd. Ik heb me ook bezig gehouden met Ryan voor een "iPhone" als behuizing voor de paper prototype. We hebben vandaag ook gezamenlijk met de klas geluncht in de studio.

#### Donderdag
"s ochtends heb ik keuzevak het keuzevak "Tools for Design" gehad van de 2e jaars peercoach Dario. Ondanks dat ik al kennis heb van Photoshop heb ik toch een paar handige dingen geleerd. Hierna hebben we op deze laatste dag hebben we het spel getest en een document gemaakt met de analyse over onze geteste game.
