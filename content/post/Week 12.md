---
title: Week 12
date: 2017-11-24
---

#### Maandag
We hebben gezamenlijk aan de debrief gewerkt en het samenwerkingscontract en SWOT analyses gemaakt.
#### Dinsdag
Vrij
#### Woensdag
Vandaag hebben we interview vragen opgesteld en een enquete samengesteld. Ook zijn we in het centrum van Rotterdam mensen geinterviewd. Hier hebben we nuttige informatie verkregen van onze doelgrep.
#### Donderdag
Zijn we naar Den Haag gegaan om interviews af te leggen voor informatie en ideeën voor ons concept. De mensen in Den Haag waren niet bereid om vragen te beantwoorden en wilde ons niet helpen. Op het einde van de dag hebben we 1 mevrouw gevonden die onze vragen wilde beantwoorden.