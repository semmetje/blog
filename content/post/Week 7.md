---
title: Week 7
date: 2017-10-13
---

#### Maandag
Naar aanleiding van de gekregen feedback zijn we aan de slag gegaan om ons huidige spelconcept te verbeteren op de punten waar het niet aan voldeed. De link met de stad Rotterdam was nog niet krachtig en de QR codes die we zouden plaatsen kunnen wegwaaien. Mijn idee was om een miniquiz toe te voegen met relevante vragen over Rotterdam. Als die quiz goed uitvoeren krijgen ze pas toegang tot het gebied doormiddel van de QR code te scannen. Deze QR code zouden we dan zo kunnen maken dat ze door de quiz een code krijgen voor het cijferslot om zo uiteindelijk het gebied te veroveren.

#### Dinsdag
Vandaag heb ik geleerd voor de Design Theory toets en een samenvatting/spiekbrief hiervoor gemaakt.

#### Woensdag
Op deze tweede studiodag heb ik aan de Miniquiz gewerkt en hebben we feedback gekregen van Carlo op de planning. Ook heb ik vandaag weer geleerd en verder gewerkt aan de samenvatting/spiekbrief.
#### Donderdag
Vanochtend had ik zoals vorige week Tools of Design: Photoshop. De opdracht van vandaag was een app waarin een profiel te zien is en takenlijst van de gebruiker. Aan het eind van de middag om 17 uur kregen we de Design Theory toets. Sommige vragen kon ik zo beantwoorden en bij andere vragen twijfelde ik tussen 2 antwoorden of wist ik helemaal niet. Hierdoor kan ik niet met zekerheid zeggen of ik deze toets heb behaald. Wachten op de beoordeling dus. 