---
title: Week 2
date: 2017-09-09
---

#### Maandag
Een nieuwe week om te werken aan project is begonnen. Vandaag hebben we een samenwerkingscontract samengesteld en gemaakt met afspraken. We hebben ook een grote mindmap gemaakt van Rotterdam.
Vervolgens hebben ik onderzoek gedaan naar verschillende opleidingen voor welke doelgroep we gaan kiezen.
Als thema leekt het ons een leuk idee om Leisure & Events Management te kiezen. Omdat dit met evenementen te maken heeft en dit ons ook leuk leek als game idee.

#### Dinsdag
Vandaag kregen we een hoorcollege over Design Theory. Deze werd gegeven door Marcos.

#### Woensdag
Op deze dag hebben we kennisgemaakt met onze Studieloopbaancoach Gerard. Ook heb ik een visueel onderzoek gemaakt van het thema Architectuur. Daarna hebben we een werkcollege gehad over wat een blog is. 
Helaas werd hier niet veel informatie gegeven, maar alleen indruk over hoe HTML werkt en dat we via markdown op een makkelijkere manier HTML konden schrijven.

#### Donderdag
Vandaag hebben we een werkcollege gehad van Design Theory. Hier moesten we een poster maken met een situatie die we hebben omgezet in een gewenst iets.

#### Vrijdag
Thuis heb ik een concept van een game verzonnen en uitgewerkt in mijn Dummy zodat ik deze maandag kan delen met mijn team.