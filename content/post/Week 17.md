---
title: Week 17
date: 2018-01-12
---

#### Maandag
Na heerlijk uitgerust te hebben van de vakantie gaan we nu weer hard aan de slag! Vandaag hebben we uiteraard verder gewerkt aan ons prototype.
#### Dinsdag
Vandaag het laatste hoorcollege van Design Theorie gehad voor het tentamen van Donderdag.
#### Woensdag
Vandaag heb ik samen met Dylan een soort van visitekaartjes gemaakt met QR codes die de bezoekers krijgen meegeleverd met de Virtual Reality bril. Ook hebben we bedacht wat we bij de expo nodig hebben en we nog moeten maken voor de presentatie van ons project.
#### Donderdag
Vanochtend heb ik het tentamen van Design Theorie gehad. Voor mijn gevoel ging deze wel goed dus hopelijk heb ik een voldoende.