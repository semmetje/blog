---
title: Week 4
date: 2017-09-22
---

#### Maandag
Het is vandaag maandag. We hebben vandaag een pitch gegeven met presentatie aan onze klas. Ik had de pitch gegeven. De klas en Carlo waren best enthousiast. We hadden feedback voor onze game gekregen. Dat hebben we gedocumenteerd en dit willen we dan verwerken in onze game. Toen kregen we plotseling te horen dat we onze "darling" moesten killen. Dit betekent dat we ons concept moeten weggooien en opnieuw een concept bedenken. Hierna hebben we nog een interview opgestelt en in gestuurd naar de Communicatie docenten.

#### Dinsdag
Weer een hoorcollege gehad over Design Theorie. Deze keer door Bob. De theorie ging over Prototyping.

#### Woensdag
Vandaag hadden we spellen meegenomen die we gingen spelen en analyseren. Ons groepje had het spel Regenwormen, 30 seconds en Weerwolven van Wakkerdam gespeeld en geanalyseerd. Vooral het spel Weerwolven van Wakkerdam was een leuk spel wat we gezamenlijk met de klas hadden gespeeld. Dit alles hebben we gedocumenteerd. Tot slot hebben Ryan en ik de conclusies getrokken uit het interview van de Communicatie studenten.

#### Donderdag
"s ochtends heb ik keuzevak het keuzevak "Tools for Design" gehad van de 2e jaars peercoach Dario. Voor de klassenopdracht moest ik verschillende dieren in een landschap verwerken. En als thuisopdracht moet ik een surrealistische afbeelding maken.
