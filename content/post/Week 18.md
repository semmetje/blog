---
title: Week 18
date: 2018-01-19
---

#### Maandag
Vandaag is het maandag. De dag om de laatste details af te maken en ervoor te zorgen dat we woensdag ons product succesvol kunnen presenteren op de EXPO. Het was even zwoegen maar we hebben alle details afgekregen.
#### Dinsdag
Vrij
#### Woensdag
Vanavond was de EXPO. Hier hebben we aan de ouders, docenten en de mensen van Fabrique ons product gepresenteerd en gevraagd aan de mensen wat ze ervan vonden. De meeste waren zeer enthousiast (wij zelf ook natuurlijk). Voordat de EXPO begon heb ik met mijn klasgenoten gezamenlijk gegeten bij Happy Italy. Het project is afgerond!

#### Einde
Ik heb dit project veel geleerd over vormgeving en Illustrator. Ook heb ik dit project veel aandacht gestoken in het testen en interviewen van mogelijke gebruikers. Uiteindelijk hebben we een product neergezet waar ik zeer tevreden over mag zijn!
