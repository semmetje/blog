---
title: Week 1 (Introductieweek)
date: 2017-09-02
---

#### Woensdag
Vandaag hebben we een introductie gehad van de opleiding CMD. Hierin werd een presentatie gegeven over hoe het jaar eruit komt te zien. Dit was best handig omdat ik kon zien hoe de periodes zijn ingedeeld en welke dingen er dan af moeten zijn.
Hierna moesten we naar de 3e etage. Daar moesten we naar de "studio". De opdracht was om een teamnaam te verzinnen en een mascotte te maken van verschillende spullen die daar lagen.
De teamnaam die we verzonnen hebben is MiXed. Omdat we allemaal verschillende hobby's en voorkeuren hebben. Onze mascotte bestaat uit een mixdrankje gemaakt van een PVC buis en dingen die normaal in een mixdrankje zitten.


#### Donderdag
 Laatste dagje school vandaag deze week. Vrijdag hoeven we gelukkig niet naar school :). Vandaag is de officiële kickoff van de eerste "Design Challenge". We kregen de opdracht te horen en kregen een hoorcollege over games. Hierna gingen we als team deelnemen aan de Rotterdam Tour. We zijn bij alle "must see" geweest en kregen zelfs nog een ijsje. Zo nu genieten van het weekend.


