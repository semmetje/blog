---
title: Week 8
date: 2017-10-27
---

#### Maandag
De laatste studiodag van dit project is vandaag. Samen met Ryan hebben we het prototype gemaakt voor de QR code houder. Deze hebben we van hout gemaakt in het stadslab. 2 houten cirkels hebben we uitgezaagd en verbonden met een scharnier wat ervoor zorgt dat de houden open en dicht kan. Als laatste hebben we 2 gaten gemaakt voor het cijferslot die we in de pauze hadden gekocht. Nadat we dit af hadden, gingen we werken aan de ontwerpproceskaart Iteratie 2 en 3.

#### Dinsdag
We hadden gisteravond een mailtje gehad van Gerhard dat we de SLC les verplaatst zou zijn naar vandaag. Naar deze SLC les ben ik gegaan met klasgenoten. Gerhard heeft een voorbeeld Starr besproken en een poging gedaan om vragen te beantwoorden.

#### Woensdag
Vandaag is de enige echte EXPO. Vanochtend vroeg gingen we naar de studio en hebben hier een tafel geclaimd voor onze spullen te presenteren. Voordat de expo begon hadden we nog een paar klene dingetjes gemaakt en geprint. Vervolgens begon de expo en hebben we om de beurt een pitch gegeven over ons spel en buiten ons eigen spel konden we kijken en vragen over de spellen van andere groepjes. Na een paar uur kregen we de uitslag wie had gewonnen. Wij hadden helaas niet gewonnen, maar gunnen het uiteraard wel de winnaars omdat zij ook een kwalitatief goed spel hadden bedacht en gemaakt.

#### Donderdag
Vanochtend had ik weer photoshop. Dit keer moesten we 3 posters maken van dieren waarin het dier gemixt is met een achtergrond. Hierna heb ik thuisgewerkt aan mijn leerdossier die morgenochtend moet worden ingeleverd. Er is veel verwarring hierover en weinig uitleg gehad tot op het laatste moment. Wens me succes..