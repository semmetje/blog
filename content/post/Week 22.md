---
title: Week 22
date: 2018-02-18
---

#### Maandag
Vandaag is het maandag. Carlo heeft vandaag de nieuwe teams voor de komende periodes ingedeeld. Deze keer zit bij Gino, Lianne, Sanne, Kayleigh en Willem. We ook al een groepsnaam bedacht en een plan van aanpak gemaakt. Onze groep heet WinX Club.
#### Dinsdag
We hebben vandaag hoorcollege gehad van Design Theory 3. Dit deed me een beetje denken aan geschiedenis. In de avond had ik het keuzevak Financiël Rekenkunde met Excel.
#### Woensdag
Op deze woensdag hadden we een korte presentatie gegeven van ons plan van aanpak aan Carlo. Visueel was die goed gemaakt alleen ontbrak onze posten op bepaalde vlakken. Dus hebben Willem en ik interviews gedaan van studenten. Hier hebben we nuttige informatie van gekregen voor ons project.
#### Donderdag
Vanochtend had ik ondersteuningsmodule Tekenen Intermiddiate. Hier moesten we letten op toonwaardes en vormen. Hierna heb ik nog wat markers aangeschaft die nodig zijn voor de volgende les. Ook heb ik samen met mijn groepje gewerkt aan de plan van aanpak.